package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class SumAccountBalance extends Specification{

    @Unroll
    def "should sum and return sum for all accounts"(){
        given: "initial data"
            def bank = new Bank()
            def accounts = SampleData.accountsList
            bank.addSampleAccounts(accounts)
        when: "sum balance from all account"
            def result = bank.sumAccountsBalance()
        then: "check sum"
            result == 1000
    }
}
