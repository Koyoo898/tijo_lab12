package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class DeleteAccountSpec  extends Specification {

    @Unroll
    def "should delete account #accountNumber"(){
        given: "initial data"
            def bank = new Bank()
            def accounts = SampleData.accountsList
            bank.addSampleAccounts(accounts)
        when: "delete account"
            def result = bank.deleteAccount(accountNumber)
        then: "check deleted account"
            result == accountBalance

        where:
            accountNumber   |   accountBalance
                1           |    50
                2           |    0
                3           |    150
                4           |    800
    }
}
