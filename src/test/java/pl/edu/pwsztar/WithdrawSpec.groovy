package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class WithdrawSpec extends Specification{

    @Unroll
    def "should withdraw #money of #accountBalance from account #accountNumber"(){
        given: "initial data"
            def bank = new Bank()
            def accounts = SampleData.accountsList
            bank.addSampleAccounts(accounts)
        when: "withdraw cash"
            def result = bank.withdraw(accountNumber, money)
        then: "check if withdraw was successful"
            result
        where:
        accountNumber   |   money   | accountBalance
            1           |   10      |   50
            2           |   0       |   0
            3           |   50      |   150
            4           |   248     |   800
    }
}
