package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class TransferSpec extends Specification{

    @Unroll
    def"should transfer from #sourceAccount to #destinyAccount account cash #cash"(){
        given: "initial data"
            def bank = new Bank()
            def accounts = SampleData.accountsList
            bank.addSampleAccounts(accounts)
        when: "transfer money"
            def result = bank.transfer(sourceAccount, destinyAccount, cash)
        then: "check if transfer is successful"
            result
        where:
            sourceAccount   |   destinyAccount  |   cash
                3           |         1         |   125
                1           |         2         |   5
                4           |         1         |   762
                4           |         2         |   460
    }
}
