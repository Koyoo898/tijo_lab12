package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class AccountBalanceSpec extends Specification{

    @Unroll
    def "should return accountBalance #accountBalance for account #account"(){
        given: "initial data"
            def bank = new Bank()
            def accounts = SampleData.accountsList
            bank.addSampleAccounts(accounts)
        when: "get account balance"
            def result = bank.accountBalance(account)
        then: "check balance"
            result == accountBalance
        where:
            account  |  accountBalance
               1     |       50
               2     |       0
               3     |       150
               4     |       800
    }
}
