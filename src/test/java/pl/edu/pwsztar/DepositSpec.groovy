package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class DepositSpec extends Specification{

    @Unroll
    def "should deposid cash #cash to account #accountNumber"(){
        given: "initial data"
            def bank = new Bank()
            def accounts = SampleData.accountsList
            bank.addSampleAccounts(accounts)
        when: "deposit cash"
            def result = bank.deposit(accountNumber, cash)
        then: "check if deposit is successful"
            result
        where:
            accountNumber   |   cash
                1           |   150
                2           |   1252
                3           |   12
                4           |   350
    }
}
