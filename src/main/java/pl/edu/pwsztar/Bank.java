package pl.edu.pwsztar;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

class Bank implements BankOperation {

    private static int accountNumber = 0;
    private final List<Account> accountList = new ArrayList<Account>();

    public void addSampleAccounts(List<Account> accounts){
        accountList.addAll(accounts);
        accountNumber = accounts.size();
    }

    public int createAccount() {
        return ++accountNumber;
    }

    public int deleteAccount(int accountNumber) {

        Optional<Account> account = findAccount(accountNumber);
        return account.isEmpty() ? BankOperation.ACCOUNT_NOT_EXISTS:account.get().getAccountBalance();
    }

    public boolean deposit(int accountNumber, int amount) {
        if(amount < 0){
            return false;
        }
        Optional<Account> account = findAccount(accountNumber);
        if(account.isEmpty()){
            return false;
        }
        else {
            account.get().depositMoney(amount);
            return true;
        }
    }

    public boolean withdraw(int accountNumber, int amount) {
        Optional<Account> account = findAccount(accountNumber);
        if(account.isEmpty()){
            return false;
        }

        if(amount <0 || amount > account.get().getAccountBalance()){
            return false;
        }
        account.get().withdrawMoney(amount);
        return true;
    }

    public boolean transfer(int fromAccount, int toAccount, int amount) {

        Optional<Account> accountFrom = findAccount(fromAccount);
        Optional<Account> accountTo = findAccount(toAccount);
        if(accountFrom.isEmpty() || accountTo.isEmpty() || fromAccount == toAccount){
            return false;
        }
        if(amount <0 || amount > accountFrom.get().getAccountBalance()){
            return false;
        }

        accountFrom.get().withdrawMoney(amount);
        accountTo.get().depositMoney(amount);
        return true;

    }

    public int accountBalance(int accountNumber) {
        Optional<Account> account = findAccount(accountNumber);
        return account.isEmpty() ? BankOperation.ACCOUNT_NOT_EXISTS : account.get().getAccountBalance();
    }

    public int sumAccountsBalance() {
        return accountList.stream().mapToInt(Account::getAccountBalance).sum();
    }


    private Optional<Account> findAccount(int accountNumber) {
        for (Account account : accountList) {
            if (account.getAccountNumber() == accountNumber) {
                return Optional.of(account);
            }
        }
            return Optional.empty();
        }
}
