package pl.edu.pwsztar;

public class Account {
    private final int accountNumber;
    private int accountBalance;

    public Account(int accountNumber, int accountBalance) {
        this.accountNumber = accountNumber;
        this.accountBalance = accountBalance;
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public int getAccountBalance() {
        return accountBalance;
    }


    public void depositMoney(int accountBalance){
        this.accountBalance = accountBalance + this.accountBalance;
    }

    public void withdrawMoney(int accountBalance){
        this.accountBalance = accountBalance - this.accountBalance;
    }
}
