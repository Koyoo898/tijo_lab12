package pl.edu.pwsztar;

import java.util.ArrayList;
import java.util.List;

public class SampleData {
    public static List<Account> getAccountsList(){

        List<Account> accounts = new ArrayList<Account>();
        accounts.add(new Account(1,50));
        accounts.add(new Account(2,0));
        accounts.add(new Account(3,150));
        accounts.add(new Account(4,800));

        return accounts;
    }
}
